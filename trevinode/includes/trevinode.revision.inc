<?php
/**
 * @file
 *   Functions for reviewing revisions.
 */

/**
 * Generate an overview of all revisions of a node.
 *
 * @param $node stdClass
 *   The node to create the overview for.
 *
 * @return string
 *   The rendered overview.
 */
function trevinode_revision_overview(stdClass $node) {
  $override = FALSE;
  foreach (field_info_instances('node', $node->type) as $instance) {
    $field = field_info_field($instance['field_name']);
    if (in_array($field['type'], trevi_field_types())) {
      $source = trevi_source_load('trevinode_revision_' . $node->type . '_' . $field['field_name']);
      if ($source->enabled()) {
        $override = TRUE;
        break;
      }
    }
  }

  if ($override) {
    return drupal_get_form('trevinode_form_revision_overview', $node);
  }
  else {
    module_load_include('inc', 'node', 'node.pages');
    return node_revision_overview($node);
  }
}

/**
 * The revision selection form.
 *
 * @param &$form_state array
 *   A Drupal form state.
 * @param $node stdClass
 *   The node to select revisions from.
 *
 * @return array
 *   A Drupal form.
 */
function trevinode_form_revision_overview(array $form, array &$form_state, stdClass $node) {
  $form['nid'] = array(
    '#type' => 'value',
    '#value' => $node->nid,
  );

  $revision_ids = array();
  foreach (node_revision_list($node) as $revision) {
    $revision_ids[] = $revision->vid;
  }

  $form['trevinode_main'] = array(
    '#title' => t('Main revision'),
    '#type' => 'radios',
    '#options' => array_fill_keys($revision_ids, ''),
    '#default_value' => $revision_ids[0],
    '#required' => TRUE,
  );
  $form['trevinode_reference'] = array(
    '#title' => t('Reference revision'),
    '#type' => 'radios',
    '#options' => array_fill_keys($revision_ids, ''),
    '#default_value' => $revision_ids[1],
    '#required' => TRUE,
  );
  $form['compare_top'] = array(
    '#type' => 'submit',
    '#value' => t('Compare'),
  );
  $form['compare_bottom'] = array(
    '#type' => 'submit',
    '#value' => t('Compare'),
  );

  return $form;
}

/**
 * Form validate callback for trevinode_form_revision_overview().
 */
function trevinode_form_revision_overview_validate(array $form, array &$form_state) {
  $values = $form_state['values'];
  if ($values['trevinode_main'] == $values['trevinode_reference']) {
    form_set_error('trevinode_main', t('Select two different revisions.'));
  }
}

/**
 * Form submit callback for trevinode_form_revision_overview().
 */
function trevinode_form_revision_overview_submit(array $form, array &$form_state) {
  $values = $form_state['values'];
  trevinode_page_redirect(node_load($values['nid']), $values['trevinode_main'], $values['trevinode_reference']);
}

/**
 * Theme callback for trevinode_form_revision_overview().
 */
function theme_trevinode_form_revision_overview(array $variables) {
  $form = $variables['form'];
  $node = node_load($form['nid']['#value']);

  $revert_permission = (user_access('revert revisions') || user_access('administer nodes')) && node_access('update', $node);
  $delete_permission = (user_access('delete revisions') || user_access('administer nodes')) && node_access('delete', $node);

  $rows = array();
  $revisions = node_revision_list($node);
  foreach ($revisions as $revision) {
    $row = array();

    // Info and log.
    $path = $revision->current_vid > 0 ? "node/$node->nid" : "node/$node->nid/revisions/$revision->vid/view";
    $row[] = t('!date by !username', array(
      '!date' => l(format_date($revision->timestamp, 'small'), $path),
      '!username' => theme('username', array(
        'account' => $revision,
      )),
    )) . '<p class="revision-log">' . filter_xss($revision->log) . '</p>';

    // Radio buttons.
    $row[] = array(
      'data' => drupal_render($form['trevinode_main'][$revision->vid]),
      'class' => array('checkbox'),
    );
    $row[] = array(
      'data' => drupal_render($form['trevinode_reference'][$revision->vid]),
      'class' => array('checkbox'),
    );

    if ($revision->current_vid == 0) {
      // Operations.
      $row[] = $revert_permission ? l(t('revert'), "node/$node->nid/revisions/$revision->vid/revert") : NULL;
      $row[] = $delete_permission ? l(t('delete'), "node/$node->nid/revisions/$revision->vid/delete") : NULL;
    }
    else {
      // Current revision marker.
      $row[] = array(
        'data' => drupal_placeholder(t('current revision')),
        'colspan' => 2,
      );

      // The class to style the current revision applies to cells and not rows.
      if ($revision->current_vid > 0) {
        foreach ($row as &$cell) {
          if (is_string($cell)) {
            $cell = array(
              'data' => $cell,
            );
          }
          $cell['class'][] = 'revision-current';
        }
      }
    }

    $rows[] = $row;
  }

  unset($form['trevinode_main']['#title']);
  unset($form['trevinode_reference']['#title']);

  $header = array(
    t('Revision'),
    array(
      'data' => t('Main revision'),
      'class' => array('checkbox'),
    ),
    array(
      'data' => t('Reference revision'),
      'class' => array('checkbox'),
    ),
    array(
      'data' => t('Operations'),
      'colspan' => 2,
    ),
  );

  return drupal_render($form['compare_top']) . theme('table', array(
    'header' => $header,
    'rows' => $rows,
  )) . drupal_render_children($form);
}