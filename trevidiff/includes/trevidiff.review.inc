<?php
/**
 * @file
 *   All functions necessary to perform a diff review. All functions, with the
 *   exception of trevidiff_trevi_diff() and trevidiff_trevi_similarity(), can
 *   be used without trevi.module.
 */

/**
 * Find two strings their similarity percentage.
 *
 * @param $matrix array
 *   A trevidiff_matrix_build() result.
 * @param $string string
 *   The string to .
 * @param $reference string
 *   The reference string.
 *
 * @return integer
 *   The strings' similarity in percents.
 */
function trevidiff_similarity_percentage(array $tokens, $string, $reference) {
  $prefix = trevidiff_prefix($tokens);
  $suffix = trevidiff_suffix($tokens);
  $common = $prefix + $suffix;
  if ($matrix = trevidiff_matrix($tokens)) {
    $common += end(end($matrix));
  }
  // We define the total amount of characters as the total length of the common
  // subsequences and all characters that are different.
  $total = drupal_strlen($string) + drupal_strlen($reference) - $common;

  return (int) ($common / $total * 100);
}

/**
 * Remove identical prefix for performance improvements.
 *
 * @param $tokens array
 *   An array where -1 contains the main string's tokens, and 1 contains those
 *   of the reference string.
 *
 * @return integer
 *   The prefix length.
 */
function trevidiff_prefix(array &$tokens) {
  $prefix = 0;
  while ($tokens[-1] && $tokens[1] && reset($tokens[-1]) === reset($tokens[1])) {
    $prefix++;
    array_shift($tokens[-1]);
    array_shift($tokens[1]);
  }
  // Rearrange keys so they still represent the correct character positions.
  foreach (array(-1, 1) as $i) {
    if ($tokens[$i]) {
      $tokens[$i] = array_combine(range($prefix, $prefix + count($tokens[$i]) - 1), $tokens[$i]);
    }
  }

  return $prefix;
}

/**
 * Remove identical suffix for performance improvements.
 *
 * @param $tokens array
 *   An array where -1 contains the main string's tokens, and 1 contains those
 *   of the reference string.
 *
 * @return integer
 *   The suffix length.
 */
function trevidiff_suffix(array &$tokens) {
  $suffix = 0;
  while ($tokens[-1] && $tokens[1] && end($tokens[-1]) === end($tokens[1])) {
    $suffix++;
    array_pop($tokens[-1]);
    array_pop($tokens[1]);
  }

  return $suffix;
}

/**
 * Build an LCS matrix.
 *
 * @param $tokens array
 *   The tokens to build the matrix from as returned by trevidiff_tokens().
 *
 * @return array
 *   The LCS matrix.
 */
function trevidiff_matrix(array $tokens) {
  $matrix = array();
  foreach ($tokens[-1] as $i => $token_string) {
    foreach ($tokens[1] as $j => $token_reference) {
      if ($token_string === $token_reference) {
        $matrix[$i][$j] = (isset($matrix[$i - 1][$j - 1]) ? $matrix[$i - 1][$j - 1] : 0) + 1;
      }
      else {
        $matrix[$i][$j] = max(
          isset($matrix[$i][$j - 1]) ? $matrix[$i][$j - 1] : 0,
          isset($matrix[$i - 1][$j]) ? $matrix[$i - 1][$j] : 0
        );
      }
    }
  }

  return $matrix;
}

/**
 * Calculate common and uncommon sequences from a token array.
 *
 * @param $tokens array
 *   The tokens as returned by trevidiff_tokens().
 *
 * @return array
 *   An array with keys -1 and 1 for the main and reference string,
 *   respectively. Both items are arrays with trevidiffSequence objects.
 */
function trevidiff_sequences(array $tokens) {
  $prefix = trevidiff_prefix($tokens);
  $suffix = trevidiff_suffix($tokens);
  $matrix = trevidiff_matrix($tokens);

  // Both strings have tokens to compare.
  if ($tokens[-1] && $tokens[1]) {
    $positions = array(
      -1 => array_keys($tokens[-1]), 
      1 => array_keys($tokens[1]),
    );
    $sequences = trevidiff_sequences_common($matrix, $tokens, $positions);
    trevidiff_sequences_diff($sequences, $positions);
  }
  // The reference string has no tokens to compare.
  elseif ($tokens[-1] && !$tokens[1]) {
    $sequences = array(
      -1 => array(new trevidiffSequence(TREVIDIFF_DIFF, $prefix, count($tokens[-1]))),
      1 => array(new trevidiffSequence(TREVIDIFF_EMPTY, $prefix, 0)),
    );
  }
  // The main string has no tokens to compare.
  elseif (!$tokens[-1] && $tokens[1]) {
    $sequences = array(
      -1 => array(new trevidiffSequence(TREVIDIFF_EMPTY, $prefix, 0)),
      1 => array(new trevidiffSequence(TREVIDIFF_DIFF, $prefix, count($tokens[1]))),
    );
  }
  // Neither string has tokens to compare.
  else {
    $sequences = array(
      -1 => array(),
      1 => array(),
    );
  }

  // Add the prefix sequence.
  if ($prefix) {
    $prefix_sequence = new trevidiffSequence(TREVIDIFF_COMMON, 0, $prefix);
    array_unshift($sequences[-1], $prefix_sequence);
    array_unshift($sequences[1], clone $prefix_sequence);
  }
  // Add the suffix sequence.
  if ($suffix) {
    $sequences[-1][] = new trevidiffSequence(TREVIDIFF_COMMON, $prefix + count($tokens[-1]), $suffix);
    $sequences[1][] = new trevidiffSequence(TREVIDIFF_COMMON, $prefix + count($tokens[1]), $suffix);
  }

  return $sequences;
}

/**
 * Process an LCS matrix and find common sequences.
 *
 * @see trevidiff_sequences
 *
 * @param $matrix array
 *   The output of trevidiff_matrix().
 * @param $tokens array
 *   The $tokens parameter as returned by trevidiff_tokens().
 * @param $positions array
 *   Token positions. See trevidiff_sequences().
 *
 * @return array
 *   An array with keys -1 and 1 for the main and reference string,
 *   respectively. Both items are arrays with trevidiffSequence objects.
 */
function trevidiff_sequences_common(array $matrix, array $tokens, array $positions) {
  $sequences = array(
    -1 => array(),
    1 => array(),
  );

  // Find all common sequences by traversing the matrix, starting with the cell
  // in the bottom right corner. After every loop the 'pointer' moves one
  // position up and to the left, which is the desired behavior if the two
  // tokens for the current pointer position match. If they don't, we modify
  // the next set of coordinates by changing $i and $j.
  for ($i = end($positions[-1]), $j = end($positions[1]); $i >= $positions[-1][0] && $j >= $positions[1][0]; $i--, $j--) {
    // The tokens are common
    if ($tokens[-1][$i] === $tokens[1][$j]) {
      // There are existing sequences.
      $new = TRUE;
      if (count($sequences[-1])) {
        $key = count($sequences[-1]) - 1;
        $last = array(
          -1 => &$sequences[-1][$key],
          1 => &$sequences[1][$key],
        );
        if ($last[-1]->position - 1 == $i && $last[1]->position - 1 == $j) {
          $new = FALSE;
          $last[-1]->position--;
          $last[1]->position--;
          $last[-1]->length++;
          $last[1]->length++;
        }
      }
      if ($new) {
        $sequences[-1][] = new trevidiffSequence(TREVIDIFF_COMMON, $i);
        $sequences[1][] = new trevidiffSequence(TREVIDIFF_COMMON, $j);
      }
    }
    // The tokens are different. Check if we need to modify the loop.
    elseif ($i != $positions[-1][0] && $j != $positions[1][0]) {
      // We reached the left side of the matrix (last character of main
      // string).
      if ($i == $positions[-1][0] && $j > $positions[1][0]) {
        $i++;
      }
      // We reached the top of the matrix (last character of reference string).
      elseif ($i > $positions[-1][0] && $j == $positions[1][0]) {
        $j++;
      }
      // We're still somewhere in the middle of the matrix.
      else {
        // The main string token doesn't equal anything from the reference.
        if ($matrix[$i][$j - 1] < $matrix[$i - 1][$j]) {
          $j++;
        }
        // The reference token doesn't equal anything from the main string.
        else {
          $i++;
        }
      }
    }
  }

  // The matrix was traversed in reverse, so correct the sequence order.
  // @todo Can we traverse the matrix from start to end, so we don't have to
  // reverse?
  return array(
    -1 => array_reverse($sequences[-1]),
    1 => array_reverse($sequences[1]),
  );
}

/**
 * Add TREVIDIFF_DIFF and TREVIDIFF_EMPTY sequences.
 *
 * @see trevidiff_sequences
 * @see trevidiff_sequences_common
 *
 * @param $sequences array
 *   The output of trevidiff_sequences_common().
 * @param $positions array
 *   Token positions. See trevidiff_sequences().
 *
 * @return NULL
 */
function trevidiff_sequences_diff(array &$sequences, array $positions) {
  // There is at least one common sequence.
  if ($sequences[-1]) {
    // Add TREVIDIFF_DIFF and TREVIDIFF_EMPTY sequences between the
    // TREVIDIFF_COMMON ones.
    $count = count($sequences[-1]);
    foreach (array(-1, 1) as $i) {
      for ($k = $count - 1; $k > 0; $k--) {
        $prefix = array_slice($sequences[$i], 0, $k);
        $suffix = array_slice($sequences[$i], $k);
        $previous = $sequences[$i][$k - 1];
        $previous_end = $previous->position + $previous->length;
        if ($previous_end == $sequences[$i][$k]->position) {
          array_push($prefix, new trevidiffSequence(TREVIDIFF_EMPTY, $sequences[$i][$k]->position));
        }
        else {
          array_push($prefix, new trevidiffSequence(TREVIDIFF_DIFF, $previous_end, $sequences[$i][$k]->position - $previous_end));
        }
        $sequences[$i] = array_merge($prefix, $suffix);
      }
    }

    foreach (array(-1, 1) as $i) {
      $j = $i * -1;
      // Add TREVIDIFF_DIFF sequences before the TREVIDIFF_COMMON ones.
      if ($sequences[$i][0]->position != $positions[$i][0]) {
        array_unshift($sequences[$i], new trevidiffSequence(TREVIDIFF_DIFF, $positions[$i][0], $sequences[$i][0]->position - $positions[$i][0]));
        $type = $sequences[$j][0]->position == $positions[$j][0] ? TREVIDIFF_EMPTY : TREVIDIFF_DIFF;
        array_unshift($sequences[$j], new trevidiffSequence($type, $positions[$j][0], $sequences[$j][0]->position - $positions[$j][0]));
      }

      // Add TREVIDIFF_DIFF sequences after the TREVIDIFF_COMMON ones.
      if (end($sequences[$i])->position + end($sequences[$i])->length -1 != end($positions[$i])) {
        $position_i = end($sequences[$i])->position + end($sequences[$i])->length;
        array_push($sequences[$i], new trevidiffSequence(TREVIDIFF_DIFF, $position_i, end($positions[$i]) - $position_i + 1));
        $type = end($sequences[$j])->position + end($sequences[$j])->length -1 == end($positions[$j]) ? TREVIDIFF_EMPTY : TREVIDIFF_DIFF;
        $position_j = end($sequences[$j])->position + end($sequences[$j])->length;
        array_push($sequences[$j], new trevidiffSequence($type, $position_j, end($positions[$j]) - $position_j + 1));
      }
    }
  }
  // There are no common sequences.
  else {
    array_unshift($sequences[-1], new trevidiffSequence(TREVIDIFF_DIFF, $positions[-1][0], count($positions[-1])));
    array_unshift($sequences[1], new trevidiffSequence(TREVIDIFF_DIFF, $positions[1][0], count($positions[1])));
  }
}

/**
 * A collection of sequential characters that are common or different. Can also
 * be a placeholder sequence if $type == TREVIDIF_EMPTY.
 */
class trevidiffSequence {

  /**
   * Either TREVIDIFF_COMMON, TREVIDIFF_DIFF or TREVIDIFF_EMPTY.
   *
   * @var integer
   */
  public $type = 0;

  /**
   * The position of this character in the string this sequence is for.
   *
   * @var integer
   */
  public $position = 0;

  /**
   * The amount of characters this sequence represents.
   *
   * @var integer
   */
  public $length = 0;

  /**
   * Constructor.
   *
   * @param $type integer
   *   Either TREVIDIFF_COMMON, TREVIDIFF_DIFF or TREVIDIFF_EMPTY.
   * @param $position integer
   *   The position of this character in the string this sequence is for.
   * @param $length integer
   *   The amount of characters this sequence represents.
   */
  function __construct($type, $position, $length = 1) {
    $this->type = $type;
    $this->position = $position;
    // Empty sequences should not have a length.
    $this->length = $type == TREVIDIFF_EMPTY ? 0 : $length;
  }
}

/**
 * A treviMethod callback: compute the differences between strings.
 */
function trevidiff_trevi_diff(treviResult $result, $language, array $string, array $reference) {
  $tokens = trevidiff_tokens($string, $reference);
  $sequences = trevidiff_sequences($tokens);

  // Mark uncommon sequences in the main string.
  $positions = array_keys($tokens[-1]);
  $positions[] = end($positions) + 1;
  foreach ($sequences[-1] as $i => $sequence) {
    if ($sequence->type == TREVIDIFF_DIFF) {
      $result->matches[] = new treviResultMatch($positions[$sequence->position], $sequence->length, 'new', t('This text is new.'));
    }
  }
  // Insert uncommon sequences from the reference string into the main
  // string.
  foreach ($sequences[1] as $i => $sequence) {
    if ($sequence->type == TREVIDIFF_DIFF) {
      $text = substr(implode($reference), $sequence->position, $sequence->length);
      $result->matches[] = new treviResultMatch($positions[$sequences[-1][$i]->position], 0, 'gone', t('This text was removed.'), $text);
    }
  }
}

/**
 * A treviMethod callback: compute the similarity percentage between strings.
 */
function trevidiff_trevi_similarity_percentage(treviResult $result, $language, array $string, array $reference) {
  $tokens = trevidiff_tokens($string, $reference);
  $percentage = trevidiff_similarity_percentage($tokens, implode($string), implode($reference));
  $result->metadata['trevidiff_similarity_percentage'] = new treviMetadataResult($percentage, t('Similarity'), t('!percentage%', array(
    '!percentage' => $percentage,
  )));
}

/**
 * A treviMethod callback: compute the difference percentage between strings.
 */
function trevidiff_trevi_difference_percentage(treviResult $result, $language, array $string, array $reference) {
  $tokens = trevidiff_tokens($string, $reference);
  $percentage = 100 - trevidiff_similarity_percentage($tokens, implode($string), implode($reference));
  $result->metadata['trevidiff_difference_percentage'] = new treviMetadataResult($percentage, t('Difference'), t('!percentage%', array(
    '!percentage' => $percentage,
  )));
}

/**
 * Convert string fragments to tokens.
 *
 * @param $string array
 *   An array containing main string fragments as passed on by trevi().
 * @param $reference array
 *   An array containing reference string fragments as passed on by trevi().
 *
 * @return array
 *   An array with keys -1 and 1 for the main and reference string,
 *   respectively. Both items are arrays where keys are character positions and
 *   values are characters from the strings.
 */
function trevidiff_tokens(array $string, array $reference) {
  $tokens = array(
    -1 => $string,
    1 => $reference,
  );
  foreach ($tokens as $i => $fragments) {
    $tokens[$i] = array();
    foreach ($fragments as $offset => $fragment) {
      foreach (str_split($fragment) as $position => $character) {
        $tokens[$i][$offset + $position] = $character;
      }
    }
  }

  return $tokens;
}