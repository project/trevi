<?php
/**
 * @file
 * Text Review hook documentation.
 */

/**
 * Define review methods.
 *
 * @see treviMethod
 * @see treviCallback
 *
 * @return array
 *   An array containing treviMethod objects. May be an associative array, but
 *   keys are never being used.
 */
function hook_trevi_method_info() {
  $methods = array(
    new treviMethod(array(
      'name' => 'trevi_foo_method',
      'type' => TREVI_METADATA,
      'reference' => TRUE,
      'callback' => new treviCallback('trevi_foo'),
      'title' => t('Foo review'),
    )),
  );

  return $methods;
}

/**
 * Alter information about defined review methods.
 *
 * @param &$methods array
 *   An associative array where keys are method names and values are
 *   treviMethod objects.
 */
function hook_trevi_method_info_alter(array &$methods) {
  $methods['trevi_foo_method']->title = t('I altered this review title.');
}

/**
 * Define text sources.
 *
 * @see treviSource
 *
 * @return array
 *   An array containing treviSource objects. May be an associative
 *   array, but keys will not be used.
 */
function hook_trevi_source_info() {
  $sources = array(
    new treviSource(array(
      'description' => t('This is just an example of a text source.'),
      'method_types' => TREVI_LIST,
      'name' => 'trevi_foo_source',
      'title' => t('Text source <em>Foo</em>'),
    )),
  );

  return $sources;
}

/**
 * Alter information about defined text sources.
 *
 * @param &$sources array
 *   An associative array where keys are source names and values are
 *   treviSource objects.
 */
function hook_trevi_source_info_alter(array &$sources) {
  $sources['trevi_foo_source']->method_types = $sources['trevi_foo_source']->method_types | TREVI_METADATA;
}

/**
 * Respond to loading a text source's configuration.
 *
 * @param $source treviSource
 *
 * @return NULL
 */
function hook_trevi_source_load(treviSource $source) {
  unset($source->defusers['trevi_html']);
}

/**
 * Respond to saving a text source's configuration.
 *
 * @param $source treviSource
 *
 * @return NULL
 */
function hook_trevi_source_save(treviSource $source) {
  cache_clear_all('example_source_' . $source->name, 'cache');
}

/**
 * Respond to deleting a text source's configuration.
 *
 * @param $source treviSource
 *
 * @return NULL
 */
function hook_trevi_source_delete(treviSource $source) {
  cache_clear_all('example_source_' . $source->name, 'cache');
}

/**
 * Define defusers.
 *
 * Defusers allow Text Review to ignore certain parts of the texts it needs to
 * review, such as HTML or whitespace.
 *
 * @see treviDefuser
 * @see treviCallback
 *
 * @return array
 *   An array containing treviDefuser objects. May be an associative array, but
 *   keys are never being used.
 */
function hook_trevi_defuser_info() {
  $defusers = array(
    new treviDefuser(
      'trevi_html',
      new treviCallback('trevi_defuse_html'),
      'HTML'
    ),
    new treviDefuser(
      'trevi_whitespace',
      new treviCallback('trevi_defuse_whitespace'),
      t('Whitespace')
    ),
  );

  return $defusers;
}

/**
 * Alter information about defined defusers.
 *
 * @param &$defusers array
 *   An associative array where keys are defuser names and values are
 *   treviDefuser objects.
 */
function hook_trevi_defuser_info_alter(array &$defusers) {
  $defusers['trevi_html']->callback = new treviCallback('trevi_defuse_html_improved');
}