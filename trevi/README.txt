
ABOUT TEXT REVIEW
-----------------

Text Review is a package of modules that allows you to review/analyze texts
from various sources using various review methods. Text sources and review
methods are "pluggable", which means other modules can simply add their own.
Read more about providing your own text sources and review methods in
DEVELOPERS.txt.

INSTALLATION
------------

Text Review is both the name of the package and the module that lies at its
heart. The module can be installed like any other module, but note that Text
Review itself does nothing. You will need to enable at least one other module
that provides a text source, such as Content Text Review, and at least one
module that provides a review method, such as Blacklists & Whitelists. The
package contains several other modules that provide more text sources and
review methods.