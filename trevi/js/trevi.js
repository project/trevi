(function($) {
  Drupal.behaviors.treviResultTooltips = {
    attach: function(context) {
      $('.trevi-comment').bind('mouseover', function(event) {
        Drupal.treviTooltipShow(Drupal.settings.treviComments[this.title]);
        Drupal.treviTooltipPosition(event.pageX, event.pageY);
        Drupal.settings.treviTooltipTitle = this.title;
        this.title = '';
      }).bind('mouseout', function() {
        Drupal.treviTooltipClose();
        this.title = Drupal.settings.treviTooltipTitle;
      });
      $(document).bind('mousemove', function(event) {
        Drupal.treviTooltipPosition(event.pageX, event.pageY);
      });
    }
  }

  /**
   * Show a tooltip.
   *
   * @return null
   */
  Drupal.treviTooltipShow = function(string) {
    Drupal.treviTooltip = $(Drupal.theme('treviTooltip', string)).appendTo(document.body);
  }

  /**
   * Position the tooltip on provided coordinates.
   *
   * @return null
   */
  Drupal.treviTooltipPosition = function(x, y) {
    if (Drupal.treviTooltip) {
      Drupal.treviTooltip.css({
        left: x,
        top: y,
      })
    }
  }

  /**
   * Close the tooltip.
   *
   * @return null
   */
  Drupal.treviTooltipClose = function() {
    Drupal.treviTooltip.remove();
  }

  /**
   * Theme the tooltip.
   *
   * @param string string
   *   The text to display in the tooltip.
   *
   * @return string
   *   The themed tooltip.
   */
  Drupal.theme.prototype.treviTooltip = function(string) {
    return '<div id="trevi-tooltip">' + string + '</div>';
  };
})(jQuery);