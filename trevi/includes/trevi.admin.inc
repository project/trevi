<?php
/**
 * @file
 *   Administration pages.
 */

/**
 * List text sources.
 *
 * @return string
 *   A list of text sources themed into a table.
 */
function trevi_page_source_list() {
  drupal_add_css(drupal_get_path('module', 'trevi') . '/css/trevi.css');

  $rows = array();
  $sources = array();
  foreach (trevi_source_load_all() as $source) {
    $sources[$source->group][] = $source;
  }
  ksort($sources);
  foreach ($sources as $group => $group_sources) {
    $rows[] = array(
      array(
        'data' => $group,
        'colspan' => 3,
        'header' => TRUE,
      ),
    );
    foreach ($group_sources as $source) {
      $description = $source->description ? '<div class="description">' . $source->description . '</div>' : NULL;
      $rows[] = array(
        array(
          'data' => $source->title . $description,
          'class' => array('trevi-source-list-title'),
        ),
        $source->enabled() ? t('enabled') : NULL,
        l(t('configure'), 'admin/config/system/trevi/source/' . $source->name),
      );
    }
  }
  if (!$rows) {
    $rows[] = array(
      array(
        'data' => t('There are no text sources yet. Enable or configure modules that provide them.'),
        'colspan' => 3,
      ),
    );
  }
  $header = array(
    t('Text source'),
    t('Enabled'),
    t('Operations'),
  );

  return theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array(
      'id' => 'trevi-source-list'
    ),
  ));
}

/**
 * Create all elements for text source configuration.
 *
 * @param $form_state array
 *   An empty form state.
 * @param $source treviSource
 *   The text source to edit/add with this form.
 *
 * @return array
 *   A Drupal form.
 */
function trevi_form_source_configuration(array $form, array $form_state, treviSource $source) {
  drupal_set_title(t('Configure %title for @group', array(
    '%title' => $source->title,
    '@group' => $source->group,
  )), PASS_THROUGH);

  $methods = trevi_method_load_all();

  $form['description'] = array(
    '#value' => $source->description,
  );

  $languages = trevi_languages();
  $form['language'] = array(
    '#type' => 'item',
    '#title' => format_plural(count($source->languages), 'Language', 'Languages'),
    '#markup' => implode(', ', array_intersect_key($languages, array_flip($source->languages))),
  );

  // Blacklist and whitelist configuration.
  $form['lists'] = array(
    '#type' => 'trevi_source_list_configuration',
    '#trevi_source_name' => $source->name,
    '#description' =>  t('Blacklists flag parts of the text, such as incorrectly written words, or words that have been inserted since a previous revision. Whitelists remove all flags from the text it matches, marking incorrectly written words as correct, for instance.'),
  );

  // Metadata reviews.
  if ($source->method_types & TREVI_METADATA) {
    $metadata_options = array();
    foreach ($methods as $method) {
      if ($method->type == TREVI_METADATA && (!$method->reference || $method->reference == $source->reference)) {
        $description = $method->description ? '<div class="description">' . $method->description . '</div>' : NULL;
        $metadata_options[$method->name] = $method->title . $description;
      }
    }
    if (empty($metadata_options)) {
      $element['metadata'] = array(
        '#type' => 'item',
        '#title' => t('Metadata review'),
        '#value' => t('There are no metadata reviews yet. Enable or configure modules that provide them.'),
      );
    }
    else {
      $form['metadata'] = array(
        '#type' => 'checkboxes',
        '#title' => t('Metadata reviews'),
        '#options' => $metadata_options,
        '#default_value' => array_keys($source->methods),
      );
    }
  }

  // Defusers. We assume there is at least one.
  $defuser_options = array();
  foreach (trevi_defuser_load_all() as $defuser_name => $defuser) {
    $description = $defuser->description ? '<div class="description">' . $defuser->description . '</div>' : NULL;
    $defuser_options[$defuser_name] = $defuser->title . $description;
  }
  $form['defusers'] = array(
    '#title' => t('Ignore'),
    '#type' => 'checkboxes',
    '#default_value' => array_keys($source->defusers),
    '#options' => $defuser_options,
  );

  // Buttons.
  $form['buttons'] = array(
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Save configuration'),
    ),
    'cancel' => array(
      '#value' => l(t('Cancel'), 'admin/config/system/trevi', array(
        'attributes' => array(
          'class' => array('trevi-source-configuration-link'),
        ),
      )),
    ),
  );

  return $form;
}

/**
 * Form validate callback for trevi_form_source_configuration().
 */
function trevi_form_source_configuration_validate(array $form, array &$form_state) {
  $values = $form_state['values'];

  $blacklists = FALSE;
  foreach (trevi_method_load_all() as $method) {
    if (TREVI_BLACKLIST & $method->type && isset($values['enabled_' . $method->name]) && $values['enabled_' . $method->name]) {
      $blacklists = TRUE;
    }
  }
  if (!$blacklists && !array_filter($values['metadata'])) {
    form_set_error('', t('Enable at least one blacklist or metadata review method for reviews to have effect.'));
  }
}

/**
 * Form submit callback for trevi_form_source_configuration().
 */
function trevi_form_source_configuration_submit(array $form, array &$form_state) {
  $values = $form_state['values'];
  $source = trevi_source_load($form['lists']['#trevi_source_name']);
  trevi_source_delete($source);

  // Defusers.
  foreach ($values['defusers'] as $defuser_name => $value) {
    if ($value) {
      $source->defusers[] = trevi_defuser_load($defuser_name);
    }
  }

  // Review methods.
  $methods = array();
  foreach (trevi_method_load_all() as $method) {
    if (TREVI_LIST & $method->type && isset($values['enabled_' . $method->name]) && $values['enabled_' . $method->name]) {
      $methods[$method->name] = $values['weight_' . $method->name];
    }
  }
  asort($methods);
  $methods = array_keys($methods);
  if (isset($values['metadata'])) {
    foreach (array_filter($values['metadata']) as $method_name => $value) {
      $methods[] = $method_name;
    }
  }
  foreach ($methods as $name) {
    $source->methods[] = trevi_method_load($name);
  }

  trevi_source_save($source);
  $form_state['redirect'] = 'admin/config/system/trevi';
  drupal_set_message(t('The configuration for %title for @group has been saved.', array(
    '%title' => $source->title,
    '@group' => $source->group,
  )));
}

/**
 * Form process callback for a trevi_source_list_configuration element.
 */
function trevi_process_source_list_configuration($element) {
  $source = trevi_source_load($element['#trevi_source_name']);

  // If we don't sort by weight, existing weights will not be respected when
  // dragging rows.
  $methods = array_merge($source->methods, array_diff_key(trevi_method_load_all(), $source->methods));
  $weight = -1;
  foreach ($methods as $method) {
    if (TREVI_LIST & $method->type
    && $source->method_types & $method->type
    && (!$method->reference || $method->reference == $source->reference)) {
      $weight = 0;
      $class = isset($weights[$method->name]) ? array('trevi-method-enabled') : array('trevi-method-disabled');
      $class[] = 'trevi-method-weight';
      $element[$method->name]['weight_' . $method->name] = array(
        '#type' => 'weight', 
        '#default_value' => $weight++,
        '#delta' => count($methods),
        '#attributes' => array(
          'class' => $class,
        ),
      );
      $element[$method->name]['enabled_' . $method->name] = array(
        '#type' => 'checkbox',
        '#title' => t('Enabled'),
        '#default_value' => isset($source->methods[$method->name]), 
      );
    }
  }

  return $element;
}

/**
 * Theme callback for a trevi_source_list_configuration element.
 *
 * @return string
 *   The themed element.
 */
function theme_trevi_source_list_configuration(array $variables) {
  $element = $variables['element'];
  $methods = trevi_method_load_all();

  // Theme blacklists and whitelists into table rows.
  $languages = trevi_languages();
  $types = array(
    TREVI_BLACKLIST => t('Blacklist'),
    TREVI_WHITELIST => t('Whitelist'),
  );
  $rows = array();
  foreach (element_children($element) as $method_name) {
    $method = $methods[$method_name];
    $description = $method->description ? '<div class="description">' . $method->description . '</div>' : '';
    // Labels? Where we're going, we don't need labels.
    unset($element[$method_name]['enabled_' . $method->name]['#title']);
    $rows[] = array(
      'data' => array(
        $method->title . $description,
        drupal_render($element[$method_name]['enabled_' . $method->name]),
        array(
          'data' => drupal_render($element[$method_name]['weight_' . $method->name]),
        ),
        $types[$method->type],
        implode(', ', array_intersect_key($languages, array_flip($method->languages))),
      ),
      'class' => array('draggable'),
    );
  }
  // Display an error message if there are no list methods.
  if (empty($rows)) {
    $rows = array(
      array(
        array(
          'data' => t('There are no blacklists available for this text source. Enable or configure modules that provide them.'),
          'colspan' => 5,
        ),
      ),
    );
  }

  $header = array(
    t('Review method'),
    t('Enabled'),
    array(
      'data' => t('Weight'),
    ),
    t('Type'),
    t('Languages'),
  );

  drupal_add_tabledrag('trevi-source-configuration', 'order', 'sibling', 'trevi-method-weight');

  return theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array(
      'id' => 'trevi-source-configuration',
      ),
    )) . '<div class="description">' . $element['#description'] . '</div>';
}