<?php
/**
 * @file
 *   View trevi() results.
 */

/**
 * Render review result matches into a string.
 *
 * @param $string string
 *   The string to render the result into.
 * @param $result treviResult
 *   The output from trevi().
 *
 * @return string
 *   The rendered string.
 */
function trevi_view_matches($string, treviResult $result) {
  drupal_add_css(drupal_get_path('module', 'trevi') . '/css/trevi.css');
  drupal_add_js(drupal_get_path('module', 'trevi') . '/js/trevi.js');

  $characters = array();
  foreach (str_split($string) as $character) {
    $characters[] = new treviCharacter($character);
  }

  // Process matches. Use a buffer ($insertions) so insertions can later be
  // processed in reverse character position order.
  $insertions = array();
  foreach ($result->matches as $match) {
    // The match matches part of the text.
    if ($match->length) {
      for ($offset = 0; $offset < $match->length; $offset++) {
        $characters[$match->position + $offset]->comments[] = $match->comment;
        $characters[$match->position + $offset]->classes[] = $match->class;
      }
    }
    // The match needs to insert text.
    else {
      // An insertion for this match already exists.
      if (isset($insertions[$match->position])) {
        $character = $insertions[$match->position];
        $character->text .= $match->text;
        $character->classes[] = $match->class;
        $character->comments[] = $match->comment;
      }
      // There is no insertion for this match yet.
      else {
        $insertions[$match->position] = array();
        $insertions[$match->position] = new treviCharacter($match->text, array($match->class), array($match->comment));
      }
    }
  }

  // Merge insertions into the character array.
  krsort($insertions);
  foreach ($insertions as $position => $insertion) {
    $prefix = array_slice($characters, 0, $position);
    $suffix = array_slice($characters, $position);
    array_push($prefix, $insertion);
    $characters = array_merge($prefix, $suffix);
  }

  // Render the matches.
  $rendered = '';
  foreach ($characters as $character) {
    sort($character->comments);
    // Comments are optional, so we check for the match class.
    if ($character->classes) {
      $character->classes = array_unique($character->classes);
      $rendered .= theme('trevi_match', array(
        'character' => $character,
      ));
      trevi_add_js_comments($character);
    }
    else {
      $rendered .= $character->text;
    }
  }

  return $rendered;
}

/**
 * A character (or characters) with extra metadata
 */
class treviCharacter {

  /**
   * The character(s) this instance represents.
   *
   * @var string
   */
  public $text = '';

  /**
   * Human-readable comments to describe $text.
   *
   * @var array
   */
  public $comments = array();

  /**
   * Machine-readable classes to describe $text. They are also used as CSS
   * classes by default.
   *
   * @var array
   */
  public $classes = array();

  /**
   * Constructor.
   *
   * @param $text string
   *   The character(s) this instance represents.
   * @param $classes array
   *   Machine-readable classes to describe $text.
   * @param $classes array
   *   Human-readable comments to describe $text.
   *
   * @return NULL
   */
  function __construct($text, array $classes = array(), array $comments = array()) {
    $this->text = $text;
    $this->classes = $classes;
    $this->comments = $comments;
  }
}

/**
 * Render review metadata.
 *
 * @param $result treviResult
 *   The result object from which to render the medata.
 *
 * @return string
 *   An HTML table into which the metadata is rendered.
 */
function trevi_view_metadata(treviResult $result) {
  $rows = array();
  foreach ($result->metadata as $metadata) {
    $rows[] = array($metadata->title, $metadata->comment ? $metadata->comment : $metadata->value);
  }
  return theme('table', array(
    'rows' => $rows,
    'attributes' => array(
      'class' => 'trevi-metadata',
    ),
  ));
}

/**
 * Theme a match.
 *
 * @return string
 *   The matched character.
 */
function theme_trevi_match(array $variables) {
  $character = $variables['character'];

  $attributes = array(
    'class' => 'trevi-match',
  );
  foreach ($character->classes as $class) {
    $attributes['class'] .= " trevi-$class";
  }
  if ($character->comments) {
    $attributes['class'] .= " trevi-comment";
    $attributes['title'] = strip_tags(implode('. ', $character->comments));
  }

  $attributes = drupal_attributes($attributes);

  return '<span' . $attributes . '>' . $character->text . '</span>';
}

/**
 * Theme a comment.
 *
 * @return string
 *   The themed string.
 */
function theme_trevi_comments(array $variables) {
  $comments = $variables['comments'];

  $output = '';
  foreach ($comments as $comment) {
    $output .= '<p>' . filter_xss($comment) . '</p>';
  }

  return $output;
}

/**
 * Add result comments to Drupal.settings for the JavaScript tooltip.
 *
 * @param $character treviCharacter
 *   A character object to store the comments for.
 */
function trevi_add_js_comments(treviCharacter $character) {
  static $comments = array();

  if ($character->comments) {
    $plain = strip_tags(implode('. ', $character->comments));
    // Only theme and add unique sets of comments once.
    if (!in_array($plain, $comments)) {
      $comments[] = $plain;
      drupal_add_js(array(
        'treviComments' => array(
          $plain => theme('trevi_comments', array(
            'comments' => $character->comments,
          )),
        ),
      ), 'setting', 'header');
    }
  }
}