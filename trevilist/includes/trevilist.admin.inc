<?php
/**
 * @file
 *   Administration pages.
 */

/**
 * List all lists.
 *
 * @return string
 *   An HTML table with list information.
 */
function trevilist_list() {
  $rows = array(
    TREVI_BLACKLIST => array(),
    TREVI_WHITELIST => array(),
  );
  $messages = array(
    TREVI_BLACKLIST => t('There are no blacklists yet. <a href="!add_form">Add a new blacklist</a>.', array('!add_form' => url('admin/config/system/trevi/trevilist/add-blacklist'))),
    TREVI_WHITELIST => t('There are no whitelists yet. <a href="!add_form">Add a new whitelist</a>.', array('!add_form' => url('admin/config/system/trevi/trevilist/add-whitelist'))),
  );
  $languages = trevi_languages();
  foreach (trevilist_load_all() as $method) {
    $language = implode(array_intersect_key($languages, array_flip($method->languages)));
    $description = $method->description ? '<div class="description">' . $method->description . '</div>' : '';
    $row = array(
      $method->title . $description,
      $language,
    );
    $row[] = l(t('view'), 'admin/config/system/trevi/trevilist/' . $method->tlmid . '/view');
    $row[] = l(t('duplicate'), 'admin/config/system/trevi/trevilist/' . $method->tlmid . '/duplicate');
    $row[] = l(t('export'), 'admin/config/system/trevi/trevilist/' . $method->tlmid . '/export');
    if (!$method->locked) {
      $row[] = l(t('edit'), 'admin/config/system/trevi/trevilist/' . $method->tlmid . '/edit');
      $row[] = l(t('delete'), 'admin/config/system/trevi/trevilist/' . $method->tlmid . '/delete');
    }
    else {
      $row[] = array(
        'data' => t('locked'),
        'colspan' => 2,
      );
    }
    $rows[$method->type][] = $row;
  }
  foreach (array_keys($rows) as $type) {
    if (!$rows[$type]) {
      $rows[$type][] = array(
        array(
          'data' => $messages[$type],
          'colspan' => 5,
        ),
      );
    }
  }

  $header[TREVI_BLACKLIST] = array(
    t('Blacklists'),
    t('Languages'),
    array(
      'data' => t('Operations'),
      'colspan' => 5,
    ),
  );
  $header[TREVI_WHITELIST] = $header[TREVI_BLACKLIST];
  $header[TREVI_WHITELIST][0] = t('Whitelists');

  return theme('table', array(
    'header' => $header[TREVI_BLACKLIST],
    'rows' => $rows[TREVI_BLACKLIST],
  )) . theme('table', array(
    'header' => $header[TREVI_WHITELIST],
    'rows' =>  $rows[TREVI_WHITELIST],
    ));
}

function trevilist_page_add($type) {
  return drupal_get_form('trevilist_form', new trevilistMethod(array(
    'type' => $type,
  )));
}

/**
 * Add, edit, or duplicate a list.
 *
 * @param &$form_state array
 *   A Drupal form state.
 * @param $method trevilistMethod
 *   The trevilistMethod for which to display the form.
 * @param $duplicate boolean
 *   Whether to duplicate $method.
 *
 * @return array
 *   A Drupal form.
 */
function trevilist_form(array $form, array &$form_state, trevilistMethod $method, $duplicate = FALSE) {
  // Clone the list so it can be modified.
  if ($duplicate) {
    $method = clone $method;
  }
  // Locked lists cannot be edited.
  elseif ($method->locked) {
    drupal_not_found();
  }

  $form_state['redirect'] = 'admin/config/system/trevi/trevilist';
  $form['#trevilist_method'] = $method;
  $form['#trevilist_duplicate'] = $duplicate;
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => $method->title,
    '#required' => TRUE,
  );
  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => $method->description,
  );
  $languages = trevi_languages();
  unset($languages['und'], $languages['zxx']);
  $form['languages'] = array(
    '#type' => 'select',
    '#title' => t('Languages'),
    '#description' => t('Leave empty if this list is language independent.'),
    '#default_value' => $method->languages,
    '#options' => $languages,
    '#multiple' => TRUE,
    '#size' => 5,
  );
  $form['keywords'] = array(
    '#type' => 'trevilist_keywords',
    '#trevilist_method' => $method,
  );
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  $form['buttons']['update'] = array(
    '#type' => 'submit',
    '#value' => t('Add/remove keyword fields'),
    '#name' => 'update'
  );

  return $form;
}

/**
 * Form validate callback for trevilist_form().
 */
function trevilist_form_validate(array $form, array &$form_state) {
  $keywords = $form_state['values']['keywords'];

  // Make sure there's at least one keyword.
  $keywords_exist = FALSE;
  $i = 0;
  while (isset($keywords['keyword_' . $i])) {
    if (strlen($keywords['keyword_' . $i])) {
      $keywords_exist = TRUE;
    }
    $i++;
  }
  if (!$keywords_exist) {
    form_set_error('', t('At least one keyword is required.'));
  }

  // Validate regular expressions.
  $i = 0;
  while (isset($keywords['keyword_' . $i])) {
    if (strlen($keywords['keyword_' . $i]) && $keywords['match_' . $i] == TREVILIST_PCRE) {
      if (@preg_match($keywords['keyword_' . $i], '') === FALSE) {
        form_set_error('keywords][keyword_' . $i, t('%pattern is not a valid <a href="@url">Perl-compatible regular expression</a>.', array(
          '%pattern' => $keywords['keyword_' . $i],
          '@url' => 'http://php.net/manual/en/reference.pcre.pattern.syntax.php',
        )));
      }
    }
    $i++;
  }
}

/**
 * Form submit callback for trevilist_form().
 */
function trevilist_form_submit(array $form, array &$form_state) {
  $values = $form_state['values'];
  $method = $form['#trevilist_method'];
  if ($form['#trevilist_duplicate']) {
    $method->tlmid = 0;
    $method->locked = FALSE;
  }
  $method->keywords = array();
  $method->title = $values['title'];
  $method->description = $values['description'];
  $method->languages = empty($values['languages']) ? array('zxx') : $values['languages'];

  $keywords = $values['keywords'];
  $i = 0;
  while (isset($keywords['keyword_' . $i])) {
    if (strlen($keywords['keyword_' . $i])) {
      $comment = isset($keywords['comment_' . $i]) ? $keywords['comment_' . $i] : NULL;
      $method->keywords[] = new trevilistKeyword($keywords['keyword_' . $i], $comment, $keywords['match_' . $i]);
    }
    $i++;
  }

  if ($form_state['clicked_button']['#name'] == 'update') {
    $form_state['rebuild'] = TRUE;
  }
  else {
    trevilist_save($method);
    drupal_set_message(t('%title has been saved.', array('%title' => $method->title)));
  }
}

/**
 * Delete a trevilistMethod.
 *
 * @param &$form_state array
 *   A Drupal form state.
 * @param $method trevilistMethod
 *   The trevilistMethod for which to display the form.
 *
 * @return array
 *   A Drupal form.
 */
function trevilist_form_delete(array &$form_state, trevilistMethod $method) {
  $form_state['redirect'] = 'admin/config/system/trevi/trevilist';
  $form['#trevilist_method'] = $method;
  $question = $method->type == TREVI_BLACKLIST ? t('Are you sure you want to delete blacklist %title?', array('%title' => $method->title)) : t('Are you sure you want to delete whitelist %title?', array('%title' => $method->title));

  return confirm_form($form, $question, 'admin/config/system/trevi/trevilist/', NULL, t('Delete'));
}

/**
 * Form submit callback for trevilist_form_delete().
 */
function trevilist_form_delete_submit(array $form, array &$form_state) {
  $method = $form['#trevilist_method'];
  trevilist_delete($method->tlmid);
  drupal_set_message(t('%title has been deleted.', array('%title' => $method->title)));
}

/**
 * Import a trevilistMethod.
 *
 * @return array
 *   A Drupal form.
 */
function trevilist_form_import() {
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Override title'),
    '#description' => t('Optionally use a different title.'),
  );
  $form['code'] = array(
    '#type' => 'textarea',
    '#title' => t('Paste review method code'),
    '#required' => TRUE,
  );
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import'),
  );

  return $form;
}

/**
 * Form validate callback for trevilist_form_import().
 */
function trevilist_form_import_validate(array $form, array &$form_state) {
  $values = &$form_state['values'];

  // Validate the existence of a method.
  if (strlen($values['code']) && !$values['method'] = trevilist_import($values['code'])) {
    form_set_error('code', t('The code contains no valid review method.'));
  }
  else {
    // Override the title.
    if (strlen($values['title'])) {
      $values['method']->title = $values['title'];
    }
    // Machine names need to be unique.
    if ($values['method']->name) {
      if (db_result(db_query("SELECT COUNT(1) FROM {trevilist} WHERE name = '%s'", $values['method']->name))) {
        form_set_error('code', t("The method's machine name is already in use."));
      }
    }
  }
}

/**
 * Form submit callback for trevilist_form_import().
 */
function trevilist_form_import_submit(array $form, array &$form_state) {
  $method = $form_state['values']['method'];
  if (is_object($method)) {
    trevilist_save($method);
    drupal_set_message(t('%title has been imported.', array('%title' => $method->title)));
  }
  $form_state['redirect'] = 'admin/config/system/trevi/trevilist';
}

/**
 * Form process callback for trevilist_keywords elements.
 */
function trevilist_form_process_trevilist_keywords(array $element) {
  $method = $element['#trevilist_method'];
  $i = -1;
  if (count($method->keywords())) {
    foreach ($method->keywords() as $keyword) {
      $i++;
      trevilist_keyword_elements($element, $i, $method, $keyword);
    }
  }
  foreach (range($i + 1, $i + 10) as $j) {
      trevilist_keyword_elements($element, $j, $method);
  }

  $element['#description'] = $method->type == TREVI_BLACKLIST ? t('Any words or phrases that should not exist in texts, with additional comments, such as possible improvements.') : t('Any words or phrases that are always allowed in texts.');
  $element['#tree'] = TRUE;

  return $element;
}

/**
 * Generate the form elements for one trevilistKeyword object.
 *
 * @param &$element array
 *   The trevilist_keywords element to expand.
 * @param $i integer
 *   The ID of this keyword/set of elements.
 * @param $Method trevilistMethod
 *   The method this $keyword belongs to.
 * @param $keyword trevilistKeyword
 *   The trevilistKeyword for which to display the form.
 *
 * @return NULL
 */
function trevilist_keyword_elements(array &$element, $i, trevilistMethod $method, trevilistKeyword $keyword = NULL) {
  $element['keyword_' . $i] = array(
    '#type' => 'textfield',
    '#default_value' => $keyword ? $keyword->keyword : NULL,
    '#size' => 30,
  );
  $element['match_' . $i] = array(
    '#type' => 'select',
    '#default_value' => $keyword ? $keyword->type : TREVILIST_CASE_INSENSITIVE,
    '#options' => array(
      TREVILIST_CASE_INSENSITIVE => t('Case insensitive'),
      TREVILIST_CASE_SENSITIVE => t('Case sensitive'),
      TREVILIST_PCRE => t('PCRE'),
    ),
  );
  if ($method->type == TREVI_BLACKLIST) {
    $element['comment_' . $i] = array(
      '#type' => 'textarea',
      '#default_value' => $keyword ? $keyword->comment : NULL,
      '#rows' => 1,
    );
  }
}

/**
 * Form theme callback; theme a trevilist_keywords element.
 */
function theme_trevilist_keywords(array $variables) {
  $element = $variables['element'];

  $i = 0;
  $rows = array();
  while (isset($element['keyword_' . $i])) {
    unset($element['keyword_' . $i]['#printed'], $element['match_' . $i]['#printed']);
    $row = array(
      drupal_render($element['keyword_' . $i]),
      drupal_render($element['match_' . $i]),
    );
    if (isset($element['comment_' . $i])) {
    unset($element['comment_' . $i]['#printed']);
      $row[] = drupal_render($element['comment_' . $i]);
    }
    $rows[] = $row;

    $i++;
  }
  $header = array(t('Keyword'), t('Match type'));
  if ($element['#trevilist_method']->type == TREVI_BLACKLIST) {
    $header[] = t('Comment');
  }
  $element['#children'] =  theme('table', array(
    'header' => $header,
    'rows' => $rows,
  ));

  return theme('form_element', array(
    'element' => $element,
    ));
}

/**
 * Export a trevilistMethod as a file download.
 *
 * @param $method trevilistMethod
 *   The method to export.
 *
 * @return NULL
 */
function trevilist_page_export(trevilistMethod $method) {
  drupal_add_http_header('Content-Type', 'text/plain; charset=UTF-8');
  drupal_add_http_header('Content-Disposition', 'attachment; filename=' . trevilist_export_filename($method->title));

  echo trevilist_export($method);
}

/**
 * Return an array with characters that cannot appear in filenames.
 *
 * @return array
 */
function trevilist_export_filename_illegal_characters() {
  return array(' ', '/', '\\', '?', '%', '*', ':', '|', '\"', '<', '>', '.');
}

/**
 * Create the filename for an exported method.
 *
 * @param $source string
 *   The string to use for the filename. The method's title for instance.
 *
 * @return string
 *   The filename.
 */
function trevilist_export_filename($source) {
  return str_replace(trevilist_export_filename_illegal_characters(), '-', strtolower(trim($source))) . '.txt';
}

/**
 * View a trevilistMethod's configuration.
 *
 * @param $method trevilistMethod
 *   The method to view.
 */
function trevilist_view($method) {
  $description = array(
    '#type' => 'item',
    '#title' => t('Description'),
    '#markup' => $method->description,
  );
  $languages = array(
    '#type' => 'item',
    '#title' => t('Languages'),
    '#markup' => theme('item_list', array(
      'items' => array_intersect_key(trevi_languages(), array_flip($method->languages)),
    )),
  );
  $types = array(
    TREVILIST_CASE_INSENSITIVE => t('Case insensitive'),
    TREVILIST_CASE_SENSITIVE => t('Case sensitive'),
    TREVILIST_PCRE => t('PCRE'),
  );
  $rows = array();
  foreach ($method->keywords() as $keyword) {
    $rows[] = array($keyword->keyword, $types[$keyword->type], $keyword->comment);
  }

  $header = array(t('Keyword'), t('Match type'), t('Comment'));
  
  return drupal_render($description) . drupal_render($languages) . theme('table', array(
    'header' => $header,
    'rows' => $rows,
  ));
}