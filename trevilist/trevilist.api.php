<?php
/**
 * @file
 * Blacklists & Whitelists hook documentation.
 */

/**
 * Respond to loading a trevilistMethod.
 *
 * @param $method trevilistMethod
 *
 * @return NULL
 */
function hook_trevilist_load(trevilistMethod $method) {
  foreach ($method->keywords() as $keyword) {
    // ...
  }
}

/**
 * Respond to saving a trevilistMethod.
 *
 * @param $method trevilistMethod
 *
 * @return NULL
 */
function hook_trevilist_save(trevilistMethod $method) {
  cache_clear_all('example_trevilist_' . $method->tlmid, 'cache');
}

/**
 * Respond to deleting a trevilistMethod.
 *
 * @param $method trevilistMethod
 *
 * @return NULL
 */
function hook_trevilist_delete(trevilistMethod $method) {
  cache_clear_all('example_trevilist_' . $method->tlmid, 'cache');
}